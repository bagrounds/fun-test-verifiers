/**
 * fun-test-verifiers is a collection of verifier functions for fun-test.
 *
 * @module fun-test-verifiers
 */
;(function () {
  'use strict'

  /* imports */
  var error = require('./error/index.js')
  var output = require('./output/index.js')

  /* exports */
  module.exports = {
    error: error,
    output: output
  }
})()

