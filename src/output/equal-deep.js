/**
 *
 * @module equal-deep
 */
;(function () {
  'use strict'

  /* imports */
  var deepEqual = require('deep-equal')

  /* exports */
  module.exports = equalDeep

  /**
   *
   * @function equalDeep
   * @alias equal-deep
   *
   * @param {String} comparison to test deep equality against
   * @return {Function} a verifier function
   */
  function equalDeep (comparison) {
    return function verifier (options, reporter) {
      if (!deepEqual(options.output, comparison)) {
        var message = 'Expected deep equality, but '
        message += JSON.stringify(comparison) + ' does not deeply equal '
        message += JSON.stringify(options.output)

        var error = new Error(message)
      }

      reporter(error)
    }
  }
})()

