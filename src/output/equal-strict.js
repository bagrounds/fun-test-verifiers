/**
 *
 * @module equal-strict
 */
;(function () {
  'use strict'

  /* imports */

  /* exports */
  module.exports = equalStrict

  /**
   *
   * @function equalStrict
   * @alias equal-strict
   *
   * @param {String} comparison to test strict equality against
   * @return {Function} a verifier function
   */
  function equalStrict (comparison) {
    return function verifier (options, reporter) {
      if (options.output !== comparison) {
        var message = 'Expected strict equality, but '
        message += comparison + ' !== ' + options.output

        var error = new Error(message)
      }

      reporter(error)
    }
  }
})()

