/**
 * present verifies that an output is present.
 *
 * @module present
 */
;(function () {
  'use strict'

  /* imports */

  /* exports */
  module.exports = present

  /**
   * present verifies that an output is present.
   *
   * @function present
   *
   * @param {Object} options all function parameters
   * @param {Object} options.output returned by the function under test
   * @param {Function} reporter handle verifier results
   */
  function present (options, reporter) {
    if (!options.output) {
      var error = new Error('Expected output to be present.')
    }

    reporter(error)
  }
})()

