/**
 *
 * @module equal-deep-strict
 */
;(function () {
  'use strict'

  /* imports */
  var deepEqual = require('deep-equal')

  /* exports */
  module.exports = equalDeepStrict

  /**
   *
   * @function equalDeepStrict
   * @alias equal-deep-strict
   *
   * @param {String} comparison to test deep equality against
   * @return {Function} a verifier function
   */
  function equalDeepStrict (comparison) {
    return function verifier (options, reporter) {
      if (!deepEqual(options.output, comparison, {strict: true})) {
        var message = 'Expected deep strict equality, but '
        message += JSON.stringify(comparison)
        message += ' does not strictly, deeply equal '
        message += JSON.stringify(options.output)

        var error = new Error(message)
      }

      reporter(error)
    }
  }
})()

