/**
 *
 * @module equal
 */
;(function () {
  'use strict'

  /* imports */

  /* exports */
  module.exports = equal

  /**
   *
   * @function equal
   *
   * @param {String} comparison to test equality against
   * @return {Function} a verifier function
   */
  function equal (comparison) {
    return function verifier (options, reporter) {
      if (options.output != comparison) {
        var message = 'Expected (non-strict) equality, but '
        message += comparison + ' != ' + options.output

        var error = new Error(message)
      }

      reporter(error)
    }
  }
})()

