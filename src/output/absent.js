/**
 * absent verifies that an output is absent.
 *
 * @module absent
 */
;(function () {
  'use strict'

  /* imports */

  /* exports */
  module.exports = absent

  /**
   * absent verifies that an output is absent.
   *
   * @function absent
   *
   * @param {Object} options all function parameters
   * @param {Object} options.output returned by the function under test
   * @param {Function} reporter handle verifier results
   */
  function absent (options, reporter) {
    if (options.output) {
      var error = new Error('Expected output to be absent.')
    }

    reporter(error)
  }
})()

