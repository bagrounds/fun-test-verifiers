/**
 * type verifies that an output is of a specific type using the type-check
 * library.
 *
 * @module type
 */
;(function () {
  'use strict'

  /* imports */
  var typeCheck = require('type-check').typeCheck

  /* exports */
  module.exports = type

  /**
   * type verifies that an output is of a specific type
   *
   * @function type
   *
   * @param {String} type as defined by the type-check module
   * @return {Function} a verifier function
   */
  function type (type) {
    return function verifier (options, reporter) {
      if (!typeCheck(type, options.output)) {
        var error = new Error('Expected output to be type: ' + type)
      }

      reporter(error)
    }
  }
})()

