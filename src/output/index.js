;(function () {
  'use strict'

  /* imports */
  var present = require('./present.js')
  var absent = require('./absent.js')
  var type = require('./type.js')
  var equalStrict = require('./equal-strict.js')
  var equal = require('./equal.js')
  var equalDeep = require('./equal-deep.js')
  var equalDeepStrict = require('./equal-deep-strict.js')

  /* exports */
  module.exports = {
    present: present,
    absent: absent,
    type: type,
    equalStrict: equalStrict,
    equal: equal,
    equalDeep: equalDeep,
    equalDeepStrict: equalDeepStrict
  }
})()

