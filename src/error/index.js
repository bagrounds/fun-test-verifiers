;(function () {
  'use strict'

  /* imports */
  var present = require('./present.js')
  var absent = require('./absent.js')

  /* exports */
  module.exports = {
    present: present,
    absent: absent
  }
})()

