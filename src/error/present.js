/**
 * present verifies that an error is present.
 *
 * @module present
 */
;(function () {
  'use strict'

  /* imports */

  /* exports */
  module.exports = present

  /**
   * present verifies that an error is present.
   *
   * @function present
   * @alias present
   *
   * @param {Object} options all function parameters
   * @param {Object} options.error returned by the function under test
   * @param {Function} reporter handle verifier results
   */
  function present (options, reporter) {
    if (!options.error) {
      var error = new Error('Expected error to be present.')
    }

    reporter(error)
  }
})()

