/**
 * absent verifies that an error is absent.
 *
 * @module absent
 */
;(function () {
  'use strict'

  /* imports */
  var VError = require('verror')

  /* exports */
  module.exports = absent

  /**
   * absent verifies that an error is absent.
   *
   * @function absent
   * @alias absent
   *
   * @param {Object} options all function parameters
   * @param {Object} options.error returned by the function under test
   * @param {Function} reporter handle verifier results
   */
  function absent (options, reporter) {
    if (options.error) {
      var error = new VError(options.error, 'Expected error to be absent.')
    }

    reporter(error)
  }
})()

